package virushack.tillcontrol.productscanner

data class Product(
    val ean: String,
    val name: String,
    val weight: Int,
    val price: Double,
    var qty: Int = 1
)
