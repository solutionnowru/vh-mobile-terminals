package virushack.tillcontrol.productscanner

import android.Manifest
import android.content.Context.VIBRATOR_SERVICE
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.add
import androidx.lifecycle.Lifecycle
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.BarcodeFormat
import com.jakewharton.rxbinding3.view.clicks
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.BarcodeView
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import virushack.base.autoDispose
import virushack.base.openAppSettings
import virushack.base.retain.retain
import virushack.base.viewbindingholder.ViewBindingHolder
import virushack.base.viewbindingholder.ViewBindingHolderImpl
import virushack.base.viewbindingholder.initBinding
import virushack.tillcontrol.Basket
import virushack.tillcontrol.OnSwipeTouchListener
import virushack.tillcontrol.ShowQrCodeFragment
import virushack.tillcontrol.databinding.FragmentProductScannerBinding
import virushack.tillcontrol.databinding.LiProductBinding
import java.util.concurrent.TimeUnit

class ProductScannerFragment : Fragment(),
    ViewBindingHolder<FragmentProductScannerBinding> by ViewBindingHolderImpl() {

    companion object {
        private const val CAMERA_REQUEST_CODE = 1001
    }

    private var bottomSheetBehavior: BottomSheetBehavior<View>? = null
    private val canRecognize
        get() = !isHidden
                && lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)
                && (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_HIDDEN
                || bottomSheetBehavior?.state == BottomSheetBehavior.STATE_COLLAPSED)

    private var _scanner: BarcodeView? = null

    private val viewModel by retain { ProductScannerViewModel() }

    private val scanCallback = object : BarcodeCallback {
        override fun barcodeResult(result: BarcodeResult) {
            if (canRecognize) {
                viewModel.codeScanned.accept(result.text)
            }

            binding?.scanner?.postDelayed({
                binding.scanner.decodeSingle(this)
            }, 1_000)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = initBinding(FragmentProductScannerBinding.inflate(inflater, container, false)) {

        val productAdapter = ProductAdapter(::onBindProduct)

        bottomSheet.products.adapter = productAdapter

        _scanner = scanner.apply {
            decoderFactory =
                DefaultDecoderFactory(listOf(BarcodeFormat.EAN_13, BarcodeFormat.EAN_8))
        }

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet.root)

        root.post {
            bottomSheet.root.layoutParams.height = root.height - totalPrice.bottom - 12
            bottomSheet.root.requestLayout()
            bottomSheetBehavior!!.setPeekHeight(root.height - recipesLabel.bottom)
        }

        arrayOf(

            totalPrice.clicks()
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .subscribe {
                    viewModel.addRandomProduct()
                },

            bottomSheet.cancelBuyButton.clicks()
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .subscribe {
                    Basket.reset()
                },

            bottomSheet.nextButton.clicks()
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .subscribe {
                    parentFragmentManager.commit {
                        hide(this@ProductScannerFragment)
                        add<ShowQrCodeFragment>(id)
                        addToBackStack("ProductScanner")
                    }
                },

            viewModel.vibrateCommand
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { vibrate() },

            viewModel.error
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Snackbar
                        .make(root, it, Snackbar.LENGTH_SHORT)
                        .show()
                },

            viewModel.state
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    progress.visibility =
                        if (it == ProductScannerViewModel.VisualState.IN_PROGRESS) View.VISIBLE
                        else View.GONE
                },

            viewModel.products
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    productAdapter.products = it

                    bottomSheet.root.post {
                        bottomSheetBehavior?.apply {
                            if (it.isEmpty()) {
                                isHideable = true
                                state = BottomSheetBehavior.STATE_HIDDEN
                            } else if (state == BottomSheetBehavior.STATE_HIDDEN) {
                                isHideable = false
                                state = BottomSheetBehavior.STATE_COLLAPSED
                            }
                        }
                    }
                },

            viewModel.totalPrice
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { totalPrice.text = it }

        ).autoDispose(viewLifecycleOwner, Lifecycle.Event.ON_DESTROY)
    }

    private fun onBindProduct(binding: LiProductBinding, product: Product) = binding.run {
        name.text = product.name
        weight.text = "${product.weight} г"
        price.text = "${product.price}"
        qty.text =
            if (product.qty > 1) "x ${product.qty}"
            else ""

        root.setOnTouchListener(object : OnSwipeTouchListener(root.context) {
            override fun onSwipeLeft() {
                viewModel.onDecreaseProductCount(product)
            }

            override fun onSwipeRight() {
                viewModel.onIncreaseProductCount(product)
            }
        })
    }

    override fun onStart() {
        super.onStart()

        _scanner!!.decodeSingle(scanCallback)

        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_REQUEST_CODE)
        }
    }

    override fun onResume() {
        super.onResume()

        _scanner!!.resume()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)

        if (hidden) {
            _scanner!!.pause()
        } else {
            _scanner!!.resume()
            _scanner!!.decodeSingle(scanCallback)
        }
    }

    override fun onPause() {
        super.onPause()

        _scanner!!.pause()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == CAMERA_REQUEST_CODE
            && (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED)
        ) {
            AlertDialog.Builder(requireContext())
                .setTitle("Отсутствует доступ к камере")
                .setMessage("Для работы сканера приложению необходимо дать разрешение на использование камеры.\nОткрыть настройки?")
                .setPositiveButton("ДА") { _, _ ->
                    requireActivity().openAppSettings()
                }
                .setNegativeButton("ВЫХОД") { _, _ ->
                    requireActivity().finish()
                }
                .show()
        }
    }

    private fun vibrate() {
        val vibrator = requireContext().getSystemService(VIBRATOR_SERVICE) as Vibrator

        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(150, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            @Suppress("DEPRECATION")
            vibrator.vibrate(150)
        }
    }
}
