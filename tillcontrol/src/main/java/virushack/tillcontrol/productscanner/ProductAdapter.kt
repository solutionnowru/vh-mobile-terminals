package virushack.tillcontrol.productscanner

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import virushack.tillcontrol.databinding.LiProductBinding
import kotlin.properties.Delegates
import kotlin.random.Random

class ProductAdapter(
    private val onBind: (LiProductBinding, Product) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var products: List<Product> by Delegates.observable(emptyList()) { _, oldList, newList ->
        autoNotify(oldList, newList) { o, n -> o.ean == n.ean }
    }

    override fun getItemCount(): Int = products.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ProductHolder(
            LiProductBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        onBind(
            (holder as ProductHolder).binding.apply {
                val colors = arrayOf(
                    "#A8A0FF",
                    "#FD8CFF",
                    "#FFCB44",
                    "#70CBFF"
                )
                val colorIndex = Random.nextInt(colors.size)
                icon.setImageDrawable(ColorDrawable(Color.parseColor(colors[colorIndex])))
            },
            products[position]
        )
    }

    private class ProductHolder(val binding: LiProductBinding) :
        RecyclerView.ViewHolder(binding.root)
}

fun <T> RecyclerView.Adapter<*>.autoNotify(
    oldList: List<T>,
    newList: List<T>,
    compare: (T, T) -> Boolean// = { old, new -> old == new }
) {
    val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return compare(oldList[oldItemPosition], newList[newItemPosition])
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }

        override fun getOldListSize() = oldList.size
        override fun getNewListSize() = newList.size
    })

    diff.dispatchUpdatesTo(this)
}
