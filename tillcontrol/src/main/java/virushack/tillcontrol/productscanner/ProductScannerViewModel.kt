package virushack.tillcontrol.productscanner

import com.jakewharton.rxrelay2.PublishRelay
import com.microsoft.windowsazure.mobileservices.table.query.QueryOperations
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import io.reactivex.rxkotlin.toCompletable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import virushack.base.toSingle
import virushack.tillcontrol.Basket
import virushack.tillcontrol.azure.AzureService
import virushack.tillcontrol.data.ScanItem
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class ProductScannerViewModel {
    enum class VisualState {
        DEFAULT,
        IN_PROGRESS
    }

    private var _randomProducts: List<Product> = emptyList()

    private val _allProductsTable =
        AzureService.client.getSyncTable("products", ScanItem::class.java)

    private val _state = BehaviorSubject.create<VisualState>()
    private val _error = PublishSubject.create<String>()
    private val _codeScanned = PublishRelay.create<String>()
    private val _vibrateCommand = PublishSubject.create<Unit>()

    val state: Observable<VisualState> = _state

    val error: Observable<String> = _error

    val vibrateCommand: Observable<Unit> = _vibrateCommand

    val products = Basket.products

    val codeScanned: Consumer<String> = _codeScanned

    val totalPrice = Basket.totalPrice

    init {
        _codeScanned
            .throttleFirst(2, TimeUnit.SECONDS)
            .subscribe(::onCodeScanned)

        _state.onNext(VisualState.IN_PROGRESS)

        _allProductsTable
            .pull(null)
            .toCompletable()
            .subscribeOn(Schedulers.io())
            .subscribe {
                _state.onNext(VisualState.DEFAULT)
            }

        _allProductsTable
            .read(null)
            .toSingle()
            .subscribe { items, _ ->
                if (items.isNullOrEmpty())
                    return@subscribe

                _randomProducts = items.map {
                    Product(it.ean13, it.name, 150, it.price)
                }.toList()
            }
    }

    private fun onCodeScanned(text: String) {
        _vibrateCommand.onNext(Unit)

        val code = text.substring(0 until text.lastIndex)

        Basket.addProduct(code)
            .subscribe { errorMessage, _ ->
                if (errorMessage.isNotBlank()) {
                    _error.onNext(errorMessage)
                }
            }
    }

    fun addRandomProduct() {
        if (_randomProducts.isEmpty())
            return

        val randomIndex = Random.nextInt(_randomProducts.size)
        Basket.addProduct(_randomProducts[randomIndex].ean)
    }

    fun onDecreaseProductCount(product: Product) {
        Basket.onDecreaseProductCount(product)
    }

    fun onIncreaseProductCount(product: Product) {
        Basket.onIncreaseProductCount(product)
    }
}
