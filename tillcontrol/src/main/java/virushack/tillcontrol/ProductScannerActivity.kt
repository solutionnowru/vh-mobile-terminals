package virushack.tillcontrol

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import virushack.base.placeUnderTransparentStatusBar

class ProductScannerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_scanner)

        placeUnderTransparentStatusBar()
    }
}
