package virushack.tillcontrol

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.lifecycle.Lifecycle
import com.jakewharton.rxbinding3.view.clicks
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import virushack.base.autoDispose
import virushack.base.setInvertedOrTranslucentStatusBar
import virushack.base.setQr
import virushack.tillcontrol.terminal.TerminaController
import virushack.tillcontrol.terminal.TerminalBasketFragment
import java.util.concurrent.TimeUnit

class ShowQrCodeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_qr_code, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setInvertedOrTranslucentStatusBar(true, viewLifecycleOwner)

        val qrImage = view.findViewById<ImageView>(R.id.qrCodeView)
        val backButton = view.findViewById<View>(R.id.nextButton)

        qrImage.setQr("terminal_connection_key", 128)

        arrayOf(

            qrImage.clicks()
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .subscribe {
                    GlobalScope.launch {
                        TerminaController.start()

                        Basket.currentProducts.asReversed().forEach { product ->
                            repeat(product.qty) {
                                TerminaController.addProduct(product.ean)
                                delay(200)
                            }
                        }
                    }

                    parentFragmentManager.commit {
                        remove(this@ShowQrCodeFragment)
                        add<TerminalBasketFragment>(id)
                        addToBackStack(null)
                    }
                },

            backButton.clicks()
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .subscribe { requireActivity().onBackPressed() }

        ).autoDispose(viewLifecycleOwner, Lifecycle.Event.ON_DESTROY)
    }
}
