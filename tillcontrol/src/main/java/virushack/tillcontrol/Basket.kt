package virushack.tillcontrol

import com.jakewharton.rxrelay2.BehaviorRelay
import com.microsoft.windowsazure.mobileservices.table.query.QueryOperations
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.Observables
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.SingleSubject
import virushack.base.toSingle
import virushack.tillcontrol.azure.AzureService
import virushack.tillcontrol.data.ScanItem
import virushack.tillcontrol.productscanner.Product
import kotlin.random.Random

object Basket {
    private val allProductsTable =
        AzureService.client.getSyncTable("products", ScanItem::class.java)

    private val _products = BehaviorSubject.createDefault<List<Product>>(emptyList())

    val currentProducts
        get() = _products.value!!

    val products: Observable<List<Product>> = _products
    val bagsCount = BehaviorRelay.createDefault(0)

    val totalPrice: Observable<String> = Observables
        .combineLatest(products, bagsCount) { p, b ->
            b * 5 + p.sumByDouble { product -> product.price * product.qty }
        }
        .map { String.format("%.2f ₽", it) }

    fun addProduct(code: String): Single<String> {
        val existingProduct = _products.value!!.firstOrNull { it.ean == code }

        if (existingProduct != null) {
            _products.onNext(_products.value!!.toMutableList().apply {
                remove(existingProduct)
                add(0, existingProduct.copy(qty = existingProduct.qty + 1))
            })
            return Single.just("")
        } else {
            val result = SingleSubject.create<String>()

            allProductsTable
                .read(QueryOperations.field("ean13").eq(code))
                .toSingle()
                .subscribe { items, _ ->
                    if (items == null) {
                        result.onSuccess("Неполадки с подключением. Попробуйте снова")
                        return@subscribe
                    }

                    val itemDto = items.firstOrNull()

                    if (itemDto == null) {
                        result.onSuccess("Товар не найден. Попробуйте снова")
                        return@subscribe
                    }

                    _products.onNext(_products.value!!.toMutableList().apply {
                        add(
                            0,
                            Product(
                                code,
                                itemDto.name,
                                150 + Random.nextInt(20) * 10,
                                itemDto.price
                            )
                        )
                    })

                    result.onSuccess("")
                }

            return result
        }
    }

    fun onDecreaseProductCount(product: Product) {
        val newProduct = product.copy(qty = product.qty - 1)

        _products.onNext(_products.value!!.toMutableList().apply {
            remove(product)

            if (newProduct.qty > 0) {
                add(0, newProduct)
            }
        })
    }

    fun onIncreaseProductCount(product: Product) {
        _products.onNext(_products.value!!.toMutableList().apply {
            remove(product)
            add(0, product.copy(qty = product.qty + 1))
        })
    }

    fun reset() {
        bagsCount.accept(0)
        _products.onNext(emptyList())
    }
}
