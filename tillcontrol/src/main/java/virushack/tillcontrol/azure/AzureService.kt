package virushack.tillcontrol.azure

import android.content.Context
import android.util.Log
import com.microsoft.windowsazure.mobileservices.MobileServiceClient
import com.microsoft.windowsazure.mobileservices.table.sync.localstore.ColumnDataType
import com.microsoft.windowsazure.mobileservices.table.sync.localstore.SQLiteLocalStore
import com.microsoft.windowsazure.mobileservices.table.sync.synchandler.SimpleSyncHandler
import io.reactivex.rxkotlin.toCompletable
import io.reactivex.schedulers.Schedulers

object AzureService {

    lateinit var client: MobileServiceClient

    fun initialize(url: String, context: Context) {

        client = MobileServiceClient(url, context)

        initOfflineTables()
    }

    private fun initOfflineTables() {
        try {
            val syncContext = client.syncContext
            if (syncContext.isInitialized)
                return

            val localStore =
                SQLiteLocalStore(client.context, "offlineStore", null, 1)

            val productItemDefinition = mutableMapOf(
                "deleted" to ColumnDataType.Boolean,
                "updatedAt" to ColumnDataType.String,
                "createdAt" to ColumnDataType.String,
                "version" to ColumnDataType.String,
                "id" to ColumnDataType.String,
                "storeId" to ColumnDataType.String,
                "discount" to ColumnDataType.String,
                "price" to ColumnDataType.String,
                "name" to ColumnDataType.String,
                "ean13" to ColumnDataType.String
            )

            localStore.defineTable("products", productItemDefinition)

            // Specify a sync handler for conflict resolution
            val handler = SimpleSyncHandler()

            syncContext.initialize(localStore, handler).toCompletable()
                .subscribeOn(Schedulers.io())
                .subscribe ()

        } catch (e: Exception) {
            Log.e("AzureService", "Offline init failed", e)
        }
    }
}
