package virushack.tillcontrol

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import virushack.base.autoDispose
import virushack.base.viewbindingholder.ViewBindingHolder
import virushack.base.viewbindingholder.ViewBindingHolderImpl
import virushack.base.viewbindingholder.initBinding
import virushack.tillcontrol.databinding.FragmentSuccessfulPaymentBinding
import virushack.tillcontrol.terminal.TerminaController
import virushack.tillcontrol.terminal.TerminalScreen
import java.util.concurrent.TimeUnit

class SuccessfulPaymentFragment : Fragment(),
    ViewBindingHolder<FragmentSuccessfulPaymentBinding> by ViewBindingHolderImpl() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    isEnabled = false

                    Basket.reset()
                    parentFragmentManager.popBackStack(
                        "ProductScanner",
                        FragmentManager.POP_BACK_STACK_INCLUSIVE
                    )
                }
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = initBinding(FragmentSuccessfulPaymentBinding.inflate(inflater, container, false)) {

        paymentLabel.text = "ОПЛАТА ..."
        backButton.visibility = View.GONE

        Observable.timer(6, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                paymentLabel.text = "ОПЛАТА УСПЕШНА!"
                backButton.visibility = View.VISIBLE
            }

        arrayOf(
            backButton.clicks().subscribe {
                GlobalScope.launch {
                    TerminaController.goTo(TerminalScreen.Cancel)
                }

                requireActivity().onBackPressed()
            }
        ).autoDispose(viewLifecycleOwner, Lifecycle.Event.ON_DESTROY)
    }
}
