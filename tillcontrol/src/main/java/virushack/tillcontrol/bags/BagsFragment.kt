package virushack.tillcontrol.bags

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.lifecycle.Lifecycle
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import virushack.base.autoDispose
import virushack.base.viewbindingholder.ViewBindingHolder
import virushack.base.viewbindingholder.ViewBindingHolderImpl
import virushack.base.viewbindingholder.initBinding
import virushack.tillcontrol.Basket
import virushack.tillcontrol.databinding.FragmentBagsBinding
import virushack.tillcontrol.selectpayment.SelectPaymentFragment
import virushack.tillcontrol.terminal.TerminaController
import virushack.tillcontrol.terminal.TerminalScreen

class BagsFragment : Fragment(), ViewBindingHolder<FragmentBagsBinding> by ViewBindingHolderImpl() {

    private val backpresedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            GlobalScope.launch {
                TerminaController.goTo(TerminalScreen.Back)
            }
            isEnabled = false
            requireActivity().onBackPressed()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this, backpresedCallback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = initBinding(FragmentBagsBinding.inflate(inflater, container, false)) {
        arrayOf(
            Basket.totalPrice
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { totalPrice.text = it },

            plusButton.clicks().subscribe { increaseBagCount() },
            minusButton.clicks().subscribe { decreaseBagCount() },
            skipButton.clicks().subscribe { skip() },
            nextButton.clicks().subscribe { next() },
            Basket.bagsCount.subscribe {
                bagSum.text =
                    if (it > 0) "${it*5} ₽"
                    else ""

                bagsCount.text = it.toString()
            }
        ).autoDispose(viewLifecycleOwner, Lifecycle.Event.ON_DESTROY)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        backpresedCallback.isEnabled = !hidden
    }

    // не берем пакеты
    private fun skip() {
        GlobalScope.launch {
            TerminaController.goTo(TerminalScreen.Pay)
        }

        parentFragmentManager.commit {
            hide(this@BagsFragment)
            add<SelectPaymentFragment>(id)
            addToBackStack(null)
        }
    }

    private fun next() {
        skip()
    }

    private fun increaseBagCount() {
        if (Basket.bagsCount.value!! < 20) {
            GlobalScope.launch {
                TerminaController.increaseBag()
            }
            Basket.bagsCount.accept(Basket.bagsCount.value!! + 1)
        }
    }

    private fun decreaseBagCount() {
        if (Basket.bagsCount.value!! > 0) {
            GlobalScope.launch {
                TerminaController.decreaseBag()
            }
            Basket.bagsCount.accept(Basket.bagsCount.value!! - 1)
        }
    }
}
