package virushack.tillcontrol

import android.app.Application
import virushack.base.PortraitOrientationCallback
import virushack.tillcontrol.azure.AzureService

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(PortraitOrientationCallback)

        AzureService.initialize("https://vt-x5-server.azurewebsites.net", this)
    }
}
