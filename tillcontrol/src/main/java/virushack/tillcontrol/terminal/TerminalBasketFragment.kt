package virushack.tillcontrol.terminal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.lifecycle.Lifecycle
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import virushack.base.autoDispose
import virushack.base.retain.retain
import virushack.base.viewbindingholder.ViewBindingHolder
import virushack.base.viewbindingholder.ViewBindingHolderImpl
import virushack.base.viewbindingholder.initBinding
import virushack.tillcontrol.Basket
import virushack.tillcontrol.OnSwipeTouchListener
import virushack.tillcontrol.bags.BagsFragment
import virushack.tillcontrol.databinding.FragmentTerminalBasketBinding
import virushack.tillcontrol.databinding.LiProductBinding
import virushack.tillcontrol.productscanner.Product
import virushack.tillcontrol.productscanner.ProductAdapter
import java.util.concurrent.TimeUnit

class TerminalBasketFragment : Fragment(),
    ViewBindingHolder<FragmentTerminalBasketBinding> by ViewBindingHolderImpl() {

    private val viewModel by retain { TerminalBasketViewModel() }

    private val backpressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            GlobalScope.launch {
                TerminaController.goTo(TerminalScreen.Cancel)
            }
            isEnabled = false
            requireActivity().onBackPressed()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this, backpressedCallback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = initBinding(FragmentTerminalBasketBinding.inflate(inflater, container, false)) {
        val productAdapter = ProductAdapter(::onBindProduct)

        products.adapter = productAdapter

        arrayOf(
            cancelBuyButton.clicks()
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .subscribe {
                    GlobalScope.launch {
                        TerminaController.goTo(TerminalScreen.Cancel)
                    }
                    Basket.reset()
                    parentFragmentManager.popBackStack("ProductScanner", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                },

            nextButton.clicks()
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .subscribe {
                    GlobalScope.launch {
                        TerminaController.goTo(TerminalScreen.Bags)
                    }

                    parentFragmentManager.commit {
                        hide(this@TerminalBasketFragment)
                        add<BagsFragment>(id)
                        addToBackStack(null)
                    }
                },

            viewModel.products
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    productAdapter.products = it
                },

            viewModel.totalPrice
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { totalPrice.text = it }

        ).autoDispose(viewLifecycleOwner, Lifecycle.Event.ON_DESTROY)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        backpressedCallback.isEnabled = !hidden
    }

    private fun onBindProduct(binding: LiProductBinding, product: Product) = binding.run {
        name.text = product.name
        weight.text = "${product.weight} г"
        price.text = "${product.price}"
        qty.text =
            if (product.qty > 1) "x ${product.qty}"
            else ""

        root.setOnTouchListener(object : OnSwipeTouchListener(root.context) {
            override fun onSwipeLeft() {
                GlobalScope.launch {
                    TerminaController.decreaseDelete(product.ean)
                }
                Basket.onDecreaseProductCount(product)
            }

            override fun onSwipeRight() {
                GlobalScope.launch {
                    TerminaController.addProduct(product.ean)
                }
                Basket.onIncreaseProductCount(product)
            }
        })
    }
}
