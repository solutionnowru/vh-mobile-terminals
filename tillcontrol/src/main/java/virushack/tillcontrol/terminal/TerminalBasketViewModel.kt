package virushack.tillcontrol.terminal

import virushack.tillcontrol.Basket

class TerminalBasketViewModel {
    val products = Basket.products

    val totalPrice = Basket.totalPrice
}
