package virushack.tillcontrol.terminal

import kotlinx.coroutines.suspendCancellableCoroutine
import okhttp3.*
import java.io.IOException
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

enum class TerminalScreen {
    Back,
    Bags,
    Pay,
    Cancel
}

object TerminaController {
    private const val HTTP_TYPE_JSON = "application/json"

    private var httpClient = OkHttpClient()
    private var requestBuilder = Request.Builder()

    private const val url = //"https://example.com"
        "https://vt-x5-server.azurewebsites.net/api/Сashbox?ZUMO-API-VERSION=2.0.0&cashboxToken=1456E0A5BF654BFB89747F72DAF8214B"

    suspend fun start() {
        callOperation("Start", "null")
    }

    suspend fun increaseBag() {
        callOperation("AddBag", 1.toString())
    }

    suspend fun decreaseBag() {
        callOperation("AddBag", (-1).toString())
    }

    suspend fun addProduct(ean: String) {
        callOperation("Add", "\"$ean\"")
    }

    suspend fun decreaseDelete(ean: String) {
        callOperation("Delete", "\"$ean\"")
    }

    suspend fun goTo(screen: TerminalScreen) {
        callOperation(
            "NavigateTo", when (screen) {
                TerminalScreen.Back -> "\"back\""
                TerminalScreen.Bags -> "\"bags\""
                TerminalScreen.Cancel -> "\"cancel\""
                TerminalScreen.Pay -> "\"pay\""
            }
        )
    }

    private suspend fun callOperation(operation: String, parameter: String) {
        performPostRequest(
            """
            {
              "Operation":"$operation",
              "Parameter":$parameter
            }
            """.trimIndent()
        )
    }

    private suspend fun performPostRequest(body: String) {
        val request = requestBuilder
            .post(RequestBody.create(null, body))
            .url(url)
            .addHeader("content-type", HTTP_TYPE_JSON)
            .build()

        val response = httpClient.newCall(request).enqueueSuspendable()
    }
}

suspend fun Call.enqueueSuspendable(): Response {
    return suspendCancellableCoroutine {
        enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                it.resume(response)
            }

            override fun onFailure(call: Call, e: IOException) {
                it.resumeWithException(e)
            }
        })
    }
}
