package virushack.tillcontrol.data

import com.google.gson.annotations.SerializedName

class ScanItem (
    @SerializedName("deleted")
    var deleted: Boolean,

    @SerializedName("updatedAt")
    var updatedAt: String,

    @SerializedName("createdAt")
    var createdAt: String,

    @SerializedName("version")
    var version: String,

    @SerializedName("id")
    var id: String,

    @SerializedName("storeId")
    var storeId: String,

    @SerializedName("discount")
    var discount: Double,

    @SerializedName("price")
    var price: Double,

    @SerializedName("name")
    var name: String,

    @SerializedName("ean13")
    var ean13: String
)
