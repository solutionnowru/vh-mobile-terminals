package virushack.tillcontrol.selectpayment

enum class PaymentType {
    BANK_CARD,
    LOYALTY,
    GOOGLE_PAY
}

data class PaymentData(val iconId: Int, val title: String, val type: PaymentType)