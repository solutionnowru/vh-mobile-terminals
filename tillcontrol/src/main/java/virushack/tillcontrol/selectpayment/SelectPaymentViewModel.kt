package virushack.tillcontrol.selectpayment

import android.util.Log
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import virushack.tillcontrol.R

class SelectPaymentViewModel {
    val items = BehaviorSubject.createDefault<List<PaymentData>>(mutableListOf(
        PaymentData(R.drawable.card1, "БАНКОВСКАЯ КАРТА", PaymentType.BANK_CARD),
        PaymentData(R.drawable.card2, "ВЫРУЧАЙ КАРТА", PaymentType.LOYALTY),
        PaymentData(R.drawable.card3, "GOOGLE PAY", PaymentType.GOOGLE_PAY)
    ))

    val totalPrice : Observable<Int> = BehaviorSubject.createDefault(1200)

    fun onClicked(item: PaymentData) {
        Log.i("SelectPaymentViewModel", "You clicked on ${item.title}")
    }
}