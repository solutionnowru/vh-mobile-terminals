package virushack.tillcontrol.selectpayment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import virushack.base.HorizontalMarginItemDecoration
import virushack.base.autoDispose
import virushack.base.helpers.UiHelper
import virushack.base.retain.retain
import virushack.base.underdevelopment.UnderDevelopmentFragment
import virushack.base.viewbindingholder.ViewBindingHolder
import virushack.base.viewbindingholder.ViewBindingHolderImpl
import virushack.base.viewbindingholder.initBinding
import virushack.tillcontrol.Basket
import virushack.tillcontrol.OnSwipeTouchListener
import virushack.tillcontrol.SuccessfulPaymentFragment
import virushack.tillcontrol.databinding.FragmentChoosePaymentBinding
import virushack.tillcontrol.databinding.LiPaymentItemBinding
import virushack.tillcontrol.databinding.LiProductBinding
import virushack.tillcontrol.productscanner.Product
import virushack.tillcontrol.productscanner.ProductAdapter
import virushack.tillcontrol.terminal.TerminaController
import virushack.tillcontrol.terminal.TerminalScreen
import java.util.concurrent.TimeUnit

class SelectPaymentFragment : Fragment(),
    ViewBindingHolder<FragmentChoosePaymentBinding> by ViewBindingHolderImpl() {

    private val viewModel by retain { SelectPaymentViewModel() }
    private lateinit var adapter: PaymentAdapter

    private val backpressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            GlobalScope.launch {
                TerminaController.goTo(TerminalScreen.Back)
            }
            isEnabled = false
            requireActivity().onBackPressed()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this, backpressedCallback)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = initBinding(FragmentChoosePaymentBinding.inflate(inflater, container, false)) {

        adapter = PaymentAdapter(::onBindProduct)

        paymentsRecyclerView.adapter = adapter
        paymentsRecyclerView.layoutManager = LinearLayoutManager(context)
        paymentsRecyclerView.addItemDecoration(
            HorizontalMarginItemDecoration(
                UiHelper.dpToPixels(
                    context,
                    10
                )
            )
        )

        arrayOf(
            cancelBuyButton.clicks()
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .subscribe {
                    GlobalScope.launch {
                        TerminaController.goTo(TerminalScreen.Cancel)
                    }

                    Basket.reset()
                    parentFragmentManager.popBackStack("ProductScanner", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                },

            backButton.clicks()
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .subscribe {
                    requireActivity().onBackPressed()
                },

            viewModel.items
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    adapter.items = it;
                    adapter.notifyDataSetChanged()
                    // TODO: Может заменить на ObservableList?
                },

            Basket.totalPrice
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { totalPrice.text = it }

        ).autoDispose(viewLifecycleOwner, Lifecycle.Event.ON_DESTROY)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        backpressedCallback.isEnabled = !hidden
    }

    private fun onBindProduct(binding: LiPaymentItemBinding, item: PaymentData) = binding.run {
        binding.paymentTitle.text = item.title
        binding.paymentIcon.setImageDrawable(resources.getDrawable(item.iconId))

        binding.root.setOnClickListener {
            GlobalScope.launch {
                TerminaController.goTo(TerminalScreen.Pay)
            }

            parentFragmentManager.commit {
                hide(this@SelectPaymentFragment)
                add<SuccessfulPaymentFragment>(id)
                addToBackStack(null)
            }
        }
    }
}
