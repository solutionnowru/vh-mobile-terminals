package virushack.tillcontrol.selectpayment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import virushack.base.BaseAdapter
import virushack.tillcontrol.databinding.LiPaymentItemBinding

class PaymentAdapter(private val onBind: (LiPaymentItemBinding, PaymentData) -> Unit) : BaseAdapter<PaymentData, PaymentViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentViewHolder =
        PaymentViewHolder(
            LiPaymentItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: PaymentViewHolder, position: Int) {
        onBind(holder.binding, items!![position])
    }
}

class PaymentViewHolder(val binding: LiPaymentItemBinding) : RecyclerView.ViewHolder(binding.root)
