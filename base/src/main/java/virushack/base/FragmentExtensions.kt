package virushack.base

import android.os.Build
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner

fun Fragment.setInvertedOrTranslucentStatusBar(value: Boolean, lifecycleOwner: LifecycleOwner) {
    lifecycleOwner.lifecycle.addObserver(object : LifecycleEventObserver {
        var addOrRemoveTranslucencyOnStop = false

        override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {

            if (event == Lifecycle.Event.ON_CREATE) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requireView().invertedStatusBar = value
                    source.lifecycle.removeObserver(this)
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    val window = requireActivity().window

                    if (value && !window.translucentStatusBar) {
                        window.translucentStatusBar
                        addOrRemoveTranslucencyOnStop = true
                    } else if (!value && window.translucentStatusBar) {
                        window.translucentStatusBar = false
                    } else {
                        source.lifecycle.removeObserver(this)
                    }
                } else {
                    source.lifecycle.removeObserver(this)
                }
            } else if (event == Lifecycle.Event.ON_STOP) {
                val window = requireActivity().window

                if (addOrRemoveTranslucencyOnStop) {
                    window.translucentStatusBar = false
                    addOrRemoveTranslucencyOnStop = false
                } else {
                    window.translucentStatusBar = true
                }
            } else if (event == Lifecycle.Event.ON_DESTROY) {
                source.lifecycle.removeObserver(this)
            }
        }
    })
}

var Fragment.invertedOrTranslucentStatusBar: Boolean
    get() {
        return view?.invertedStatusBar == true || activity?.window?.translucentStatusBar == true
    }
    set(value) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requireView().invertedStatusBar = value

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val window = requireActivity().window

            window.translucentStatusBar = value
        }
    }

var View.invertedStatusBar: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
            && systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR == View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    set(value) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return

        systemUiVisibility =
            if (value) systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            else systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()

    }

var Window.translucentStatusBar: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
            && attributes.flags and WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS == WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
    set(value) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT)
            return

        if (value) {
            setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
            )
        } else if (!value) {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }
    }
