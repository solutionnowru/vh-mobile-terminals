package virushack.base

import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T1, T2>() :
    RecyclerView.Adapter<T2>() where T2 : RecyclerView.ViewHolder {
    var items: List<T1>? = null

    override fun getItemCount(): Int = items?.size ?: 0
}