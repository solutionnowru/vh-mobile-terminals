package virushack.base.helpers

import android.content.Context
import java.lang.Exception

object UiHelper {
    fun dpToPixels(context: Context?, dp: Int): Int = try {
        (context!!.resources.displayMetrics.density * dp + 0.5f).toInt()
    } catch (e: Exception) {
        dp
    }
}