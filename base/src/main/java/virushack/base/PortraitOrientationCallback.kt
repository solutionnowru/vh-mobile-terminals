package virushack.base

import android.app.Activity
import android.content.pm.ActivityInfo
import android.os.Bundle

object PortraitOrientationCallback : ActivityLifecycleCallback {
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }
}
