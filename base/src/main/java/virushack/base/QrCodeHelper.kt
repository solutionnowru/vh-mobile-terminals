package virushack.base

import android.graphics.Bitmap
import android.graphics.Color.BLACK
import android.graphics.Color.WHITE
import android.util.Log
import android.widget.ImageView
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel

fun ImageView.setQr(content: String, size: Int, padding: Int? = null) {
    try {
        val result = getQrMatrix(content, size)

        val pixels = IntArray(size * size)
        for (y in 0 until size) {
            val offset = y * size
            for (x in 0 until size) {
                pixels[offset + x] = if (result.get(x, y)) BLACK else WHITE
            }
        }

        val bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888)
        bitmap.setPixels(pixels, 0, size, 0, 0, size, size)

        this.setImageBitmap(bitmap)
    } catch (e: Exception) {
        Log.e("QrCodeHelper", "Failed to generate qr $e")
    }
}

private fun getQrMatrix(content: String, qrSize: Int): BitMatrix {
    return QRCodeWriter().encode(
        content,
        BarcodeFormat.QR_CODE,
        qrSize, qrSize,
        hashMapOf<EncodeHintType, Any>(
            EncodeHintType.ERROR_CORRECTION to ErrorCorrectionLevel.H,
            EncodeHintType.MIN_SIZE to qrSize,
            EncodeHintType.MARGIN to 0
        )
    )
}
