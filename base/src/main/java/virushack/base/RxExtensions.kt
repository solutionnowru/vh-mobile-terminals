package virushack.base

import com.google.common.util.concurrent.ListenableFuture
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T> ListenableFuture<out T>.toSingle(): Single<T> {
    return Single
        .defer {
            try {
                Single.just(get())
            } catch (t: Throwable) {
                Single.error<T>(t)
            }
        }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}
