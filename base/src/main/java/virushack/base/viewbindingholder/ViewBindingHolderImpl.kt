package virushack.base.viewbindingholder

import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.viewbinding.ViewBinding
import virushack.base.lifecyclehooks.letAfter

/**
 * Holds and manages ViewBinding inside a Fragment.
 * modified version of https://android.jlelse.eu/viewbinding-in-fragments-the-clean-easy-way-2f0ce68aee22
 */
class ViewBindingHolderImpl<T : ViewBinding> : ViewBindingHolder<T> {
    override var binding: T? = null

    private lateinit var fragmentName: String

    override fun requireBinding(block: (T.() -> Unit)?) =
        binding?.apply { block?.invoke(this) }
            ?: throw IllegalStateException("Accessing binding outside of Fragment lifecycle: $fragmentName")

    override fun initBinding(
        fragment: Fragment,
        binding: T,
        onBound: (T.() -> Unit)?
    ): View {
        this.binding = binding

        //To not leak memory
        fragment.viewLifecycleOwner.letAfter(Lifecycle.Event.ON_DESTROY) {
            this.binding = null
        }

        fragmentName = fragment::class.java.simpleName
        onBound?.invoke(binding)
        return binding.root
    }
}
